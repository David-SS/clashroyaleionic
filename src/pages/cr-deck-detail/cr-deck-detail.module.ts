import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrDeckDetailPage } from './cr-deck-detail';

@NgModule({
  declarations: [
    CrDeckDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CrDeckDetailPage),
  ],
})
export class CrDeckDetailPageModule {}
