import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrEditorPage } from './cr-editor';

@NgModule({
  declarations: [
    CrEditorPage,
  ],
  imports: [
    IonicPageModule.forChild(CrEditorPage),
  ],
})
export class CrEditorPageModule {}
