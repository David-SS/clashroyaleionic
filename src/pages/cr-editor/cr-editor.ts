import {Component} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import {Card} from "../../models/card_model";
import {Deck} from "../../models/deck_model";
import {CardDataProvider} from "../../providers/card-data/card-data";
import {DeckDataProvider} from "../../providers/deck-data/deck-data";
import {Observable} from "rxjs/RX";

@IonicPage()
@Component({
  selector: 'page-cr-editor',
  templateUrl: 'cr-editor.html',
  providers: [CardDataProvider, DeckDataProvider]
})
export class CrEditorPage {

  cardList: Card[];
  deck: Deck;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private modalCtrl: ModalController,
              private alertCtrl: AlertController,
              private toastCtrl: ToastController,
              private cardDataProvider: CardDataProvider,
              private deckDataProvider: DeckDataProvider) {
    console.log('CrEditorPage');
    this.cardList = [];
    this.deck = new Deck();
  }

  ionViewDidLoad() {
    this.deck = this.navParams.get('deck');

    let cardListObservable = this.cardDataProvider.getAllCards();
    let deckListObservable= this.deckDataProvider.getDeckById(this.navParams.get('deck').id);
    Observable.combineLatest(cardListObservable,deckListObservable)
      .subscribe(
        ([cards, deck]) => {
          this.cardList = cards;
          if (this.deck.id !== -1) {
            this.deck = deck;
          }
          this.updateCardsList();
          this.deck.stats.checkDeck(this.deck);
        },
        (error) => console.error(error)
    );
  }

  updateCardsList() {
    this.deck.cards.forEach((card) => {
      const index: number = this.cardList.findIndex(cardFromList => cardFromList.key === card.key);
      if (index !== -1) {
        this.cardList.splice(index, 1);
      }
    });
  }

  saveDeck(deck: Deck) {
    if (this.deck.id === -1) {
      this.deckDataProvider.createDeck(deck)
        .then((response) => {
          console.log('Created deck with new id...')
          const toast = this.toastCtrl.create({
            message: 'Mazo creado correctamente',
            duration: 3000
          });
          toast.present();
        });
    } else {
      this.deckDataProvider.updateDeck(deck)
        .then((response) => {
          console.log('Updated deck with id: ' + deck.id)
        });
        const toast = this.toastCtrl.create({
          message: 'Mazo actualizado correctamente',
          duration: 3000
        });
        toast.present();
    }
    this.navCtrl.popToRoot().then(() => {
      console.log('Going to root page...');
    });
  }

  deleteDeck(deck: Deck) {
    let alert = this.alertCtrl.create({
      title: '¿Eliminar el mazo?',
      message: '¿Está seguro de que desea eliminar este mazo?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Eliminar',
          handler: () => {
            this.deckDataProvider.deleteDeck(deck)
              .then((response) => {
                console.log('Deleted deck with id: ' + deck.id);
                this.navCtrl.popToRoot().then(() => {
                  console.log('Going to root...');
                });
              });
          }
        }
      ]
    });
    alert.present();
  }

  switchCardByRandom(card) {
    let randomIndex = new Date().getTime() % (this.cardList.length - 1);
    const deckCardIndex: number = this.deck.cards.findIndex(cardFromDeck => cardFromDeck.key === card.key);
    if (deckCardIndex !== -1 && randomIndex != -1) {
      this.deck.cards[deckCardIndex] = this.cardList[randomIndex];
      this.cardList.splice(randomIndex,1);
      if (card.id !== 0) this.cardList.push(card);
    }
  }

  deselectCard(card: Card) {
    this.cardList.push(card);
  }

  takeNextRandomCard() {
    let firstRandomNum = new Date().getTime() * new Date().getTime();
    let secondRandomNum = new Date().getTime() * new Date().getTime();
    let randomIndex = (firstRandomNum * secondRandomNum) % (this.cardList.length - 1);
    this.selectCard(this.cardList[randomIndex]);
    this.cardList.splice(randomIndex, 1);
  }

  generateRandomDeck() {
    for (let i = 0; i < 8; i++) {
      this.takeNextRandomCard();
    }
    let now = new Date();
    this.deck.name = 'Random_' + now.getTime().toString();
  }

  openStatsModal(deck: Deck) {
    const modalStats = this.modalCtrl.create('CrDeckStatsPage', {deck: deck});
    modalStats.present().then(() => {
      console.log('Opening stats modal...');
    });
  }

  selectCard(card: Card) {
    const removedCard = this.deck.addCard(card);
    if (removedCard !== null)  {
      this.cardList.push(removedCard);
    }
  }
}

