import {Component} from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {Clipboard} from "@ionic-native/clipboard";
import {CrEditorPage} from "../cr-editor/cr-editor";
import {Deck} from "../../models/deck_model";
import {DeckDataProvider} from "../../providers/deck-data/deck-data";
import {CardDataProvider} from "../../providers/card-data/card-data";
import {CrDeckDetailPage} from "../cr-deck-detail/cr-deck-detail";
import {Observable} from "rxjs";
import {Card} from "../../models/card_model";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [CardDataProvider, DeckDataProvider]
})
export class HomePage {

  cardList: Card[];
  deckList: Deck[];

  constructor(public navCtrl: NavController,
              private clipboard: Clipboard,
              private alertCtrl: AlertController,
              private cardDataProvider: CardDataProvider,
              private deckDataProvider: DeckDataProvider) {
    console.log('HomePage');
    this.cardList = [];
    this.deckList = [];
  }

  ionViewDidLoad() {
    let cardListObservable= this.cardDataProvider.getAllCards();
    let deckListObservable= this.deckDataProvider.getAllDecks();
    Observable.combineLatest(cardListObservable, deckListObservable)
      .subscribe(
        ([cards, decks]) => {
          this.cardList = cards;
          this.deckList = decks;
          this.deckList.forEach((deck) => {
            deck.stats.checkDeck(deck);
          });
        },
        (error) => console.error(error)
      );
  }

  goDetails(deck) {
    this.navCtrl.push(CrDeckDetailPage, {deck: deck})
      .then((response) => {
        console.log('Going details page for deck with id: ' + deck.id);
      });
  }

  goEditor(deck) {
    if (deck === -1) deck = new Deck();
    this.navCtrl.push(CrEditorPage, {deck: deck})
      .then((response) => {
        console.log('Going to editor page for deck with id: ' + deck.id);
      });
  }

  copyDeck() {
    this.clipboard.paste().then(
      (deckLink: string) => {
        let copiedDeckCards = [];
        let deckURL = deckLink.split("?");
        let deckURLParams = deckURL[1].split("&");
        deckURLParams.forEach((param) => {
          let singleURLParam = param.split('=');
          if (singleURLParam[0] === 'deck') {
            copiedDeckCards = singleURLParam[1].split(';');
          }
        });
        if (copiedDeckCards.length === 8) {
          this.generateCopiedDeck(copiedDeckCards);
        }
        //this.clipboard.clear();
      },
      (reject: string) => {
        alert('Error: ' + reject);
      }
    );
  }

  private generateCopiedDeck(stringCards: string[]) {
    let copiedDeck = new Deck();
    stringCards.forEach((stringCard) => {
      let card = this.cardList.find((card) => card.id.toString() === stringCard);
      copiedDeck.addCard(card);
    });
    this.goEditor(copiedDeck);
  }

  deleteDeck(deck) {
    let alert = this.alertCtrl.create({
      title: '¿Eliminar el mazo?',
      message: '¿Está seguro de que desea eliminar este mazo?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Eliminar',
          handler: () => {
            this.deckDataProvider.deleteDeck(deck)
              .then((response) => {
                console.log('Deleted deck with id: ' + deck.id);
              });
          }
        }
      ]
    });
    alert.present();
  }

}
