import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Slides} from 'ionic-angular';
import {Observable} from "rxjs";
import {DeckDataProvider} from "../../providers/deck-data/deck-data";
import {Deck} from "../../models/deck_model";
import {Card} from "../../models/card_model";

@IonicPage()
@Component({
  selector: 'page-cr-stats',
  templateUrl: 'cr-stats.html',
  providers: [DeckDataProvider]
})
export class CrStatsPage {

  @ViewChild(Slides) slides: Slides;

  deckList: Deck[];
  maxUsedCards: Card[];
  maxUsedCardCount: number;
  elixirCostMean: number;
  deckCount: number;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private deckDataProvider: DeckDataProvider) {
    console.log('CrStatsPage');
    this.deckList = [];
    this.maxUsedCards = [];
    this.deckCount = 0;
    this.maxUsedCardCount = 0;
    this.elixirCostMean = 0;
  }

  ionViewDidLoad() {
    let deckListObservable= this.deckDataProvider.getAllDecks();
    Observable.combineLatest(deckListObservable)
      .subscribe(
        ([decks]) => {
          this.deckList = decks;
          this.deckCount = this.deckList.length;
          this.getMaxUsedCard();
          this.getElixirCostMean();
        },
        (error) => console.error(error)
      );
  }

  getMaxUsedCard(){
    this.maxUsedCards = [];
    this.maxUsedCardCount = 0;
    let cardsCount = [];
    this.deckList.forEach((deck) => {
      deck.cards.forEach((card) => {
        if (cardsCount[card.key] === undefined) {
          cardsCount[card.key] = {'card': card, 'count': 1};
        } else {
          cardsCount[card.key] = {'card': card, 'count': cardsCount[card.key]['count'] + 1}
        }
      })
    });
    Object.keys(cardsCount).forEach((key) => {
      if (this.maxUsedCardCount < cardsCount[key]['count']) {
        this.maxUsedCards = [];
        this.maxUsedCards.push(cardsCount[key]['card']);
        this.maxUsedCardCount = cardsCount[key]['count'];
      }
      else if (this.maxUsedCardCount === cardsCount[key]['count']) {
        this.maxUsedCards.push(cardsCount[key]['card']);
      }
    });
  }

  getElixirCostMean() {
    this.elixirCostMean = 0;
    this.deckList.forEach((deck) => {
      this.elixirCostMean = this.elixirCostMean + deck.stats.getMeanElixirCost(deck);
    });
    if (this.elixirCostMean !== 0) {
      this.elixirCostMean = this.elixirCostMean / this.deckCount;
    }
  }

  nextSlide() {
    if (this.slides.isEnd()) {
      this.slides.slideTo(0);
    } else {
      this.slides.slideNext();
    }
  }

}
