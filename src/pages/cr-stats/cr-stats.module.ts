import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrStatsPage } from './cr-stats';

@NgModule({
  declarations: [
    CrStatsPage,
  ],
  imports: [
    IonicPageModule.forChild(CrStatsPage),
  ],
})
export class CrStatsPageModule {}
