import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import { Chart } from 'chart.js';
import {Deck} from "../../models/deck_model";

@IonicPage()
@Component({
  selector: 'page-cr-deck-stats',
  templateUrl: 'cr-deck-stats.html',
})
export class CrDeckStatsPage {

  checkList: Object;
  deck: Deck;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private view: ViewController) {
    console.log('ionViewDidLoad CrDeckStatsPage');
    this.deck = new Deck();
    this.checkList = {
      'winCond': false,
      'groundTroops': false,
      'airTroops': false,
      'groundTankTroops': false,
      'groundSpamTroops': false,
      'airTankTroops': false,
      'airSpamTroops': false,
      'airDmg': false,
      'areaDmg': false,
      'airAreaDmg': false,
      'buildings': false,
      'spells': false,
      'strongSpells': false,
    };
  }

  ionViewDidLoad() {
    this.deck = this.navParams.get('deck');
    this.updateStats();
  }

  closeModal() {
    this.view.dismiss().then(() => {
      console.log('Closing stats modal...')
    });
  }

  private updateStats() {
    this.deck.stats.checkDeck(this.deck);
    this.updateCheckList();
    this.initCardTypeChart();
    this.initElixirChart();
  }

  updateCheckList() {
    this.checkList['winCond'] = this.deck.stats.thereIsWinCondition;
    this.checkList['groundTroops'] = this.deck.stats.thereIsGroundTroops;
    this.checkList['airTroops'] = this.deck.stats.thereIsAirTroops;
    this.checkList['groundTankTroops'] = this.deck.stats.thereIsGroundTankTroop;
    this.checkList['groundSpamTroops'] = this.deck.stats.thereIsGroundSpamTroop;
    this.checkList['airTankTroops'] = this.deck.stats.thereIsAirTankTroop;
    this.checkList['airSpamTroops'] = this.deck.stats.thereIsAirSpamTroop;
    this.checkList['airDmg'] = this.deck.stats.thereIsAirDmg;
    this.checkList['areaDmg'] = this.deck.stats.thereIsAreaDmg;
    this.checkList['airAreaDmg'] = this.deck.stats.thereIsAirAreaDmg;
    this.checkList['buildings'] = this.deck.stats.thereIsBuilding;
    this.checkList['spells'] = this.deck.stats.thereIsSpell;
    this.checkList['strongSpells'] = this.deck.stats.thereIsStrongSpell;
  }

  CardTypeChart: any;
  @ViewChild('CardTypeCanvas') CardTypeCanvas;
  private initCardTypeChart() {
    this.CardTypeChart = new Chart(this.CardTypeCanvas.nativeElement, {
      type: 'horizontalBar',
      data: {
        labels: ['Cartas'],
        datasets: [
          {
            label: 'Tropas',
            stack: 'cards',
            data: [this.deck.stats.currentTroops],
            backgroundColor: ['rgba(255, 99, 132, 1.0)'],
            hoverBackgroundColor: ["#FF6384"]
          },
          {
            label: 'Estructuras',
            stack: 'cards',
            data: [this.deck.stats.currentBuilding],
            backgroundColor: ['rgba(54, 162, 235, 1.0)'],
            hoverBackgroundColor: ["#36A2EB"]
          },
          {
            label: 'Hechizos',
            stack: 'cards',
            data: [this.deck.stats.currentSpell],
            backgroundColor: ['rgba(255, 206, 86, 1.0)'],
            hoverBackgroundColor: ["#FFCE56"]
          }]
      },
      options: {
        legend: {
          position: 'bottom'
        },
        scales: {
          xAxes: [{ticks: { min: 0, max: 8}}],
          yAxes: [{stacked: true}]
        }
      }
    });
  }

  ElixirChart: any;
  @ViewChild('ElixirCanvas') ElixirCanvas;
  private initElixirChart() {
    this.ElixirChart = new Chart(this.ElixirCanvas.nativeElement, {
      type: 'horizontalBar',
      data: {
        labels: ['Coste'],
        datasets: [
          {
            label: 'Elixir',
            data: [this.deck.stats.getMeanElixirCost(this.deck)],
            backgroundColor: [this.getElixirColorBar()],
            hoverBackgroundColor: ["#FF6384"]
          }]
      },
      options: {
        legend: {
          position: 'bottom'
        },
        scales: {
          xAxes: [{
            ticks: { min: 0, max: 8}
          }]
        }
      }
    });
  }

  private getElixirColorBar() {
    let softElixir = 'rgba(255, 99, 132, 1.0)';
    let mediumElixir = 'rgba(223, 6, 52, 1.0)';
    let hardElixir = 'rgba(116, 2, 156, 1.0)';

    if (this.deck.stats.getMeanElixirCost(this.deck) <= 4) {
      return softElixir;
    } else if (this.deck.stats.getMeanElixirCost(this.deck) > 4 && this.deck.stats.getMeanElixirCost(this.deck) <= 6) {
      return mediumElixir;
    } else {
      return hardElixir;
    }
  }

}
