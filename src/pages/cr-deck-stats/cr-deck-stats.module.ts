import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrDeckStatsPage } from './cr-deck-stats';

@NgModule({
  declarations: [
    CrDeckStatsPage,
  ],
  imports: [
    IonicPageModule.forChild(CrDeckStatsPage),
  ],
})
export class CrDeckStatsPageModule {}
