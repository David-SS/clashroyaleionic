/**
 * Provides a Deck object
 */
import {Card} from './card_model';
import {Deck} from './deck_model';

export class DeckStats {

  constructor(
    public currentElixirCost: number = 0,
    public currentSpell: number = 0,
    public currentBuilding: number = 0,
    public currentTroops: number = 0,
    public thereIsWinCondition: boolean = false,
    public thereIsGroundTroops: boolean = false,
    public thereIsAirTroops: boolean = false,
    public thereIsGroundTankTroop: boolean = false,
    public thereIsGroundSpamTroop: boolean = false,
    public thereIsAirTankTroop: boolean = false,
    public thereIsAirSpamTroop: boolean = false,
    public thereIsAirDmg: boolean = false,
    public thereIsAreaDmg: boolean = false,
    public thereIsAirAreaDmg: boolean = false,
    public thereIsBuilding: boolean = false,
    public thereIsSpell: boolean = false,
    public thereIsStrongSpell: boolean = false,
  ) {
  }

  public getMeanElixirCost(currentDeck: Deck): number {
    this.checkDeck(currentDeck);
    if (this.currentElixirCost === 0) { return 0.0; }
    return (this.currentElixirCost / currentDeck.getCardsCount());
  }

  /**
   * Check all stats
   * @param currentDeck
   */
  public checkDeck(currentDeck: Deck) {
    this.resetStats();
    currentDeck.cards.forEach((card: Card) => {
      this.checkElixirCost(card);
      this.checkType(card);
      this.checkConditions(card);
    });
  }

  /**
   * Reset current stats
   */
  resetStats() {
      this.currentElixirCost = 0;
      this.currentSpell = 0;
      this.currentBuilding = 0;
      this.currentTroops = 0;
      this.thereIsWinCondition = false;
      this.thereIsGroundTroops = false;
      this.thereIsAirTroops = false;
      this.thereIsGroundTankTroop = false;
      this.thereIsGroundSpamTroop = false;
      this.thereIsAirTankTroop = false;
      this.thereIsAirSpamTroop = false;
      this.thereIsAirDmg = false;
      this.thereIsAreaDmg = false;
      this.thereIsAirAreaDmg = false;
      this.thereIsBuilding = false;
      this.thereIsSpell = false;
      this.thereIsStrongSpell = false;
  }

  /**
   * Increase elixir cost
   * @param card
   */
  checkElixirCost(card: Card) {
    this.currentElixirCost += card.elixir;
  }

  /**
   * Increase card type count
   * @param card
   */
  checkType(card: Card) {
    if (card.type === 'Troop') { this.currentTroops++; }
    if (card.type === 'Building') { this.currentBuilding++; }
    if (card.type === 'Spell') { this.currentSpell++; }
  }

  /**
   * Check all rest of boolean props (ideal deck conditions)
   * @param card
   */
  checkConditions(card: Card) {
    if (!this.thereIsWinCondition) {
      this.thereIsWinCondition = (card.type === 'Troop' && card.win_condition);
    }
    if (!this.thereIsGroundTroops) {
      this.thereIsGroundTroops = (card.type === 'Troop');
    }
    if (!this.thereIsAirTroops) {
      this.thereIsAirTroops = (card.type === 'Troop' && card.air_troop);
    }
    if (!this.thereIsGroundTankTroop) {
      this.thereIsGroundTankTroop = (card.type === 'Troop' && !card.air_troop && card.tank_troop);
    }
    if (!this.thereIsGroundSpamTroop) {
      this.thereIsGroundSpamTroop = (card.type === 'Troop' && !card.air_troop && card.spam_troop);
    }
    if (!this.thereIsAirTankTroop) {
      this.thereIsAirTankTroop = (card.type === 'Troop' && card.air_troop && card.tank_troop);
    }
    if (!this.thereIsAirSpamTroop) {
      this.thereIsAirSpamTroop = (card.type === 'Troop' && card.air_troop && card.spam_troop);
    }
    if (!this.thereIsAirDmg) {
      this.thereIsAirDmg = (card.type === 'Troop' && card.air_dmg);
    }
    if (!this.thereIsAreaDmg) {
      this.thereIsAreaDmg = (card.type === 'Troop' && card.area_dmg);
    }
    if (!this.thereIsAirAreaDmg) {
      this.thereIsAirAreaDmg = (card.type === 'Troop' && card.air_dmg && card.area_dmg);
    }
    if (!this.thereIsSpell) {
      this.thereIsSpell = (card.type === 'Spell');
    }
    if (!this.thereIsStrongSpell) {
      this.thereIsStrongSpell = (card.type === 'Spell' && card.strong_spell);
    }
    if (!this.thereIsBuilding) {
      this.thereIsBuilding = (card.type === 'Building');
    }
  }
}
