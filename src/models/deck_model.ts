/**
 * Provides a Deck object
 */
import {Card} from './card_model';
import {DeckStats} from './deck_stats_model';

export class Deck {

  public id: number;
  public name: string;
  public cards: Card[];
  public stats: DeckStats;
  public readonly emptyCard: Card;

  constructor(values: Object = {}) {
    Object.assign(this, values);
    this.stats = new DeckStats();
    this.emptyCard = new Card();
    if (this.id === undefined) {
      this.resetProps();
      this.resetCards();
    } else {
      // We need it: values.cards array should have its own reference
      // not copy array reference, copy its values

      this.cards = [
        new Card(this.cards[0]), new Card(this.cards[1]),
        new Card(this.cards[2]), new Card(this.cards[3]),
        new Card(this.cards[4]), new Card(this.cards[5]),
        new Card(this.cards[6]), new Card(this.cards[7])
      ];
    }
  }

  /**
   * Add new card (param) to current deck and returns first card.
   * This first card is added to card list if there is 8 cards when added
   * the new card.
   * @param card
   */
  addCard(card: Card): Card {
    for (let i = 0; i < this.cards.length; i++) {
      if (this.cards[i].id === 0) {
        this.cards[i] = card;
        return null;
      }
    }
    let oldCard;
    oldCard = this.cards[0];
    for (let i = 0; i < this.cards.length - 1; i++) { this.cards[i] = this.cards[i + 1]; }
    this.cards[this.cards.length - 1] = card;
    return oldCard;
  }

  /**
   * Remove a card in a specific position (0-7)
   * @param cardIndex
   */
  removeCard(cardIndex: number) {
    if (cardIndex !== -1) {
      this.cards[cardIndex] = this.emptyCard;
    }
  }

  /**
   * Get the current count of selected cards (all cards that are not empty cards)
   */
  getCardsCount () {
    let cardCount = 0;
    this.cards.forEach((card: Card) => {
      if (card.id !== 0) { cardCount++; }
    });
    return cardCount;
  }

  /**
   * Set current deck as a new deck
   */
  resetProps() {
    this.id = -1;
    this.name = '';
  }

  /**
   * Set current cards as empty cards
   */
  resetCards() {
    this.cards = [
      this.emptyCard, this.emptyCard,
      this.emptyCard, this.emptyCard,
      this.emptyCard, this.emptyCard,
      this.emptyCard, this.emptyCard];
  }
}
