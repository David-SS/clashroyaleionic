/**
 * Provides a Card object
 */
export class Card {
  id: number;
  key: string;
  name: string;
  type: string;
  description: string;
  rarity: string;
  elixir: number;
  arena: number;
  air_troop: boolean;
  spam_troop: boolean;
  tank_troop: boolean;
  win_condition: boolean;
  air_dmg: boolean;
  area_dmg: boolean;
  strong_spell: boolean;
  imageUrl: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
    if (this.key === undefined) {
      this.id = 0;
      this.elixir = 0;
      this.key = 'emptyCard';
      this.imageUrl = 'assets/cards/nonselected.png';
    } else {
      this.imageUrl = 'assets/cards/' + this.key + '.png';
    }
  }

  getRarityIndex() {
    if (this.rarity === 'Legendary') { return 0; }
    if (this.rarity === 'Epic') {return 1; }
    if (this.rarity === 'Rare') {return 2; }
    return 3;
  }
}
