import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import "rxjs-compat/add/operator/map";
import {Card} from "../../models/card_model";
import {Deck} from "../../models/deck_model";

import { HttpClient } from '@angular/common/http';
import {AngularFireDatabase} from "@angular/fire/database"; // <-- Para Realtime database
//import {AngularFirestore} from "@angular/fire/firestore"; <-- Para FireStoreimport { Injectable } from '@angular/core';

@Injectable()
export class FirebaseProvider {

  constructor(public http: HttpClient,
              public firebase: AngularFireDatabase) {
    console.log('FirebaseProvider');
  }

  getAllCards(): Observable<Card[]> {
    // ? <-- Para FireStore
    return this.firebase.list('/cards').valueChanges()
      //.map(changes => { cards {  <-- Alternativa
      .pipe(map(cards => {
          return cards.map((card) => new Card(card))
      }));
  }

  getAllDecks(): Observable<Deck[]> {
    // ? <-- Para FireStore
    return this.firebase.list('/decks').valueChanges()
      .pipe(map(decks => {
        return decks.map((deck) => new Deck(deck))
      }));
  }

  getDeckById(deckId): Observable<Deck> {
    // ? <-- Para FireStore
    return this.firebase.object('decks/' + deckId).valueChanges()
      .pipe(map(deck => {
        return new Deck(deck)
      }));
  }

  createDeck(deck: Deck): Promise<Deck> {
    deck.id = Date.now();
    return new Promise<any>((resolve, reject) => {
      let deckToCreate = {
        id: deck.id,
        name: deck.name,
        cards: deck.cards
      };
      //this.firebase.collection('/users').add({...}).then(...) <-- Para FireStore
      this.firebase.list('/decks').update(deck.id.toString(), deckToCreate)
        .then(
          (res) => resolve(res),
          (err) => reject(err))
    })

  }

  updateDeck(deck: Deck): Promise<Deck> {
    let deckToCreate = {
      name: deck.name,
      cards: deck.cards
    };
    return new Promise<any>((resolve, reject) => {
      //this.firebase.collection('/users').add({...}).then(...) <-- Para FireStore
      this.firebase.list('/decks').update(deck.id.toString(), deckToCreate)
        .then(
          (res) => resolve(res),
          (err) => reject(err))
    })
  }

  deleteDeck(deck: Deck): Promise<Deck> {
    return new Promise<any>((resolve, reject) => {
      //this.firebase.collection('/users').add({...}).then(...) <-- Para FireStore
      this.firebase.list('/decks').remove(deck.id.toString())
        .then(
          (res) => resolve(res),
          (err) => reject(err))
    })
  }

}
