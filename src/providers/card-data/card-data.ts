import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {FirebaseProvider} from "../firebase/firebase";
import {Card} from "../../models/card_model";


@Injectable()
export class CardDataProvider {

  constructor(private api: FirebaseProvider) {
    console.log('CardDataProvider');
  }

  getAllCards(): Observable<Card[]> {
    return this.api.getAllCards();
  }

}
