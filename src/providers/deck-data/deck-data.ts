import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {FirebaseProvider} from "../firebase/firebase";
import {Deck} from "../../models/deck_model";

@Injectable()
export class DeckDataProvider {

  constructor(private api: FirebaseProvider) {
    console.log('DeckDataProvider');
  }

  getAllDecks(): Observable<Deck[]> {
    return this.api.getAllDecks();
  }

  getDeckById(id): Observable<Deck> {
    return this.api.getDeckById(id);
  }

  createDeck(deck: Deck): Promise<Deck> {
    deck.id = Date.now();
    return this.api.createDeck(deck);
  }

  updateDeck(deck: Deck): Promise<Deck> {
    return this.api.updateDeck(deck);
  }

  deleteDeck(deck: Deck): Promise<Deck> {
    return this.api.deleteDeck(deck);
  }

}
