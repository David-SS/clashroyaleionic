import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {Clipboard} from "@ionic-native/clipboard";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {CrDeckDetailPage} from "../pages/cr-deck-detail/cr-deck-detail";
import {CrEditorPage} from "../pages/cr-editor/cr-editor";
import {CrStatsPage} from "../pages/cr-stats/cr-stats";

import {environment} from "../environments/environment";
import {HttpClientModule} from "@angular/common/http";
import {FirebaseProvider} from '../providers/firebase/firebase';
import {AngularFireModule} from "@angular/fire";
import {AngularFireDatabaseModule} from "@angular/fire/database";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {ComponentsModule} from '../components/components.module';
import { CardDataProvider } from '../providers/card-data/card-data';
import { DeckDataProvider } from '../providers/deck-data/deck-data';
import {FormsModule} from "@angular/forms";
import {CrDeckStatsPageModule} from "../pages/cr-deck-stats/cr-deck-stats.module";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CrDeckDetailPage,
    CrEditorPage,
    CrStatsPage,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    ComponentsModule,
    CrDeckStatsPageModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CrDeckDetailPage,
    CrEditorPage,
    CrStatsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Clipboard,
    FirebaseProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CardDataProvider,
    DeckDataProvider
  ]
})
export class AppModule {}
