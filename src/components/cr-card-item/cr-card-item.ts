import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {Card} from "../../models/card_model";

@Component({
  selector: 'cr-card-item',
  templateUrl: 'cr-card-item.html'
})
export class CrCardItemComponent implements OnInit, OnChanges {

  @Input() card: Card;

  constructor() {
    console.log('CrCardItemComponent');
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

}
