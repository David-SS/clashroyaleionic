import {Component, EventEmitter, Input, Output, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {Deck} from "../../models/deck_model";

@Component({
  selector: 'cr-deck-list',
  templateUrl: 'cr-deck-list.html'
})
export class CrDeckListComponent implements OnInit, OnChanges{

  @Input() deckList: Deck[];
  filteredDeckList: Deck[];

  @Output() onSelectDeckEvent: EventEmitter<Deck>;
  @Output() onEditDeckEvent: EventEmitter<Deck>;
  @Output() onDeleteDeckEvent: EventEmitter<Deck>;

  constructor() {
    console.log('CrDeckListComponent');
    this.onSelectDeckEvent = new EventEmitter();
    this.onEditDeckEvent = new EventEmitter();
    this.onDeleteDeckEvent = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.filteredDeckList = this.deckList;
  }

  searchDecks(event: any) {
    this.filteredDeckList = this.deckList;
    const searchQuery = event.target.value;
    if (searchQuery && searchQuery.trim() != '') {
      this.filteredDeckList = this.deckList.filter((deck) => {
        return (deck.name.toLowerCase().indexOf(searchQuery.toLowerCase()) > -1);
      })
    }
  }

  onSelectDeckEmitter(deck) {
    this.onSelectDeckEvent.emit(deck);
  }

  onEditDeckEmitter(deck) {
    this.onEditDeckEvent.emit(deck);
  }

  onDeleteDeckEmitter(deck) {
    this.onDeleteDeckEvent.emit(deck);
  }

}
