import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {Deck} from "../../models/deck_model";

@Component({
  selector: 'cr-deck-row',
  templateUrl: 'cr-deck-row.html'
})
export class CrDeckRowComponent implements OnInit, OnChanges{

  @Input() deck: Deck;

  constructor() {
    console.log('CrDeckRowComponent');
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

}
