import {Component, EventEmitter, Input, Output, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {Card} from "../../models/card_model";

@Component({
  selector: 'cr-card-list',
  templateUrl: 'cr-card-list.html'
})
export class CrCardListComponent implements OnInit, OnChanges {

  @Input() cardList: Card[];
  sortMode: string;

  @Output() onCardListSelectedEvent: EventEmitter<Card>;

  constructor() {
    console.log('CrCardListComponent');
    this.onCardListSelectedEvent = new EventEmitter();
    this.sortMode = '';
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  onCardListSelectedEmitter(card: Card) {
    this.popElementOfCardList(card);
    this.onCardListSelectedEvent.emit(card);
  }

  private popElementOfCardList(card: Card) {
    const index: number = this.cardList.indexOf(card);
    if (index !== -1) {
      this.cardList.splice(index, 1);
    }
  }

  sortedCardList(): Card[] {
    return this.cardList.sort((a: Card, b: Card) => {
      if (this.sortMode === 'elixir') { return CrCardListComponent.getElixirSort(a, b); }
      if (this.sortMode === 'irarity') { return CrCardListComponent.getInverseRaritySort(a, b); }
      return CrCardListComponent.getRaritySort(a, b);
    });
  }

  static getElixirSort(a: Card, b: Card) {
    let comparator: number;
    comparator = a.elixir - b.elixir;
    return comparator;
  }

  static getInverseRaritySort(a: Card, b: Card) {
    let comparator: number;
    comparator = a.getRarityIndex() - b.getRarityIndex();
    if (comparator === 0) {
      return CrCardListComponent.getElixirSort(a, b);
    } else {
      return comparator;
    }
  }

  static getRaritySort(a: Card, b: Card) {
    let comparator: number;
    comparator = b.getRarityIndex() - a.getRarityIndex();
    if (comparator === 0) {
      return CrCardListComponent.getElixirSort(a, b);
    } else {
      return comparator;
    }
  }

}
