import {Component, EventEmitter, Input, Output, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {Deck} from "../../models/deck_model";
import {Card} from "../../models/card_model";

@Component({
  selector: 'cr-deck-editor',
  templateUrl: 'cr-deck-editor.html'
})
export class CrDeckEditorComponent implements OnInit, OnChanges {

  @Input() deck: Deck;
  deckEditorOpened: boolean;

  @Output() onRandomCardSwitchEvent: EventEmitter<Card>;
  @Output() onDeckCardSelectedEvent: EventEmitter<Card>;
  @Output() onNextRandomCardEvent: EventEmitter<Card>;
  @Output() onDeckRandomGenerateEvent: EventEmitter<Card>;
  @Output() onStatsDisplayEvent: EventEmitter<Deck>;

  constructor () {
    console.log('CrDeckEditorComponent');
    this.onRandomCardSwitchEvent = new EventEmitter();
    this.onDeckCardSelectedEvent = new EventEmitter();
    this.onNextRandomCardEvent = new EventEmitter();
    this.onDeckRandomGenerateEvent = new EventEmitter();
    this.onStatsDisplayEvent = new EventEmitter();
    this.deckEditorOpened = true;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  onRandomCardSwitchEmitter(card: Card) {
    this.onRandomCardSwitchEvent.emit(card);
  }

  onDeckCardSelectedEmitter(card: Card) {
    if (card.id !== 0) {
      this.popElementOfCardList(card);
      this.onDeckCardSelectedEvent.emit(card);
    }
  }

  private popElementOfCardList(card: Card) {
    this.deck.removeCard(this.deck.cards.indexOf(card));
  }

  onNextRandomCardEmitter() {
    this.onNextRandomCardEvent.emit();
  }

  onDeckRandomGenerateEmitter() {
    this.onDeckRandomGenerateEvent.emit();
  }

  onStatsDisplayEmitter() {
    this.onStatsDisplayEvent.emit(this.deck);
  }

  openCloseDeckEditor() {
    this.deckEditorOpened = !this.deckEditorOpened;
  }

}
