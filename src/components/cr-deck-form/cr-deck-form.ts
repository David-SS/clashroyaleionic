import {Component, EventEmitter, Input, Output,  OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {Deck} from "../../models/deck_model";

@Component({
  selector: 'cr-deck-form',
  templateUrl: 'cr-deck-form.html'
})
export class CrDeckFormComponent implements OnInit, OnChanges {

  @Input() deck: Deck;

  @Output() onDeckSaveEvent: EventEmitter<Deck>;
  @Output() onDeckDeleteEvent: EventEmitter<Deck>;

  constructor() {
    console.log('CrDeckFormComponent');
    this.onDeckSaveEvent = new EventEmitter();
    this.onDeckDeleteEvent = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  saveDeckEmitter() {
    if (this.deck.name.length > 0 && this.deck.getCardsCount() === 8) {
      this.onDeckSaveEvent.emit(this.deck);
    }
  }

  deleteDeckEmitter() {
    this.onDeckDeleteEvent.emit(this.deck);
  }
}
