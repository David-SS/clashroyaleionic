import { NgModule } from '@angular/core';
import { CrDeckListComponent } from './cr-deck-list/cr-deck-list';
import { CrDeckRowComponent } from './cr-deck-row/cr-deck-row';
import { CrDeckEditorComponent } from './cr-deck-editor/cr-deck-editor';
import { CrCardListComponent } from './cr-card-list/cr-card-list';
import { CrCardItemComponent } from './cr-card-item/cr-card-item';
import { CrDeckFormComponent } from './cr-deck-form/cr-deck-form';
import {IonicModule} from "ionic-angular";
@NgModule({
	declarations: [CrDeckListComponent,
    CrDeckRowComponent,
    CrDeckEditorComponent,
    CrCardListComponent,
    CrCardItemComponent,
    CrDeckFormComponent],
	imports: [ IonicModule ],
	exports: [CrDeckListComponent,
    CrDeckRowComponent,
    CrDeckEditorComponent,
    CrCardListComponent,
    CrCardItemComponent,
    CrDeckFormComponent]
})
export class ComponentsModule {}
